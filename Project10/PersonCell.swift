//
//  PersonCell.swift
//  Project10
//
//  Created by JONATHAN HOLLAND on 7/30/19.
//  Copyright © 2019 Jonny. All rights reserved.
//

import UIKit

class PersonCell: UICollectionViewCell {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var name: UILabel!
}
