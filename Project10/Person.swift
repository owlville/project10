//
//  Person.swift
//  Project10
//
//  Created by JONATHAN HOLLAND on 7/31/19.
//  Copyright © 2019 Jonny. All rights reserved.
//

import UIKit

class Person: NSObject {
    var name: String
    var image: String
    
    init(name: String, image: String) {
        self.name = name
        self.image = image
    }
}
